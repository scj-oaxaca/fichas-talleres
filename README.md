# Fichas técnicas de los talleres de la SCJ

En este repositoriose encuentra el código para generar un pdf sencillo que sirva como presentación de los talleres de la sede Oaxaca de la Sociedad Científica Juvenil.

La plantilla fue proporcionada por la profesora Erika Pérez Rivera, profesora ayudante en la Facultad de Ciencias de la UNAM, quien cuenta con una amplia experiencia en la divulgación científica y su profecionalización.

## Requisitos

El script que genera el pdf está escrito en Python y hace uso de la biblioteca pylatex, por lo cual debe asegurarse que tiene instalado tanto python 3.x (aparentemente funciona con python 2 pero no ha sido probado) y pylatex.

Python está integrado en casi todos los sistemas operativos modernos, por lo cual es muy probable que ya lo tenga instalado. 

Para instalar pylatex se recomienda hacerlo mediante pip, que necesita ser instalada también. Para ello en distribuciones basadas en ArchLinux se utiliza

```
sudo pacman -Syu
sudo pacman -S python-pip
```
mientras que en distribuciones basadas en debian lo instala mediante 
```
sudo apt update
sudo apt install python3-pip
```
Si su sistema operativo es Windows o MacOS puede hacer una búsqueda rápida en algún buscador como [DuckDuckGo](www.duckduckgo.com) o [Google](www.google.com) para saber cómo instalar pip en sus sistema operativo.

Una vez tenga instalado pip puede instalar pylatex fácilmente mediante el comando
```
pip install pylatex
```
Con lo cual tendrá cubierto todos los requerimientos para este script.

## Cómo funciona
La versión actual del script es muy simple y sólo requiere que se coloque en el mismo directorio que el script un archivo llamado talleres.csv con los datos de los talleres que se quieran compilar. Así, lo único que debe hacerse es ejecutar la siguiente orden en una terminal abierta en el directorio de ambos archivos
```
python fichas.py
```

### Archivo talleres.csv
El archivo tallleres.csv se puede obtener de la srespuestas al [formulario](https://forms.gle/7WmqVP5Q6gPsUroF8) creado para ello.

## Contribuir
Puede ayudar a mejorar el código del script aportando sus propias ideas a los desarrolladores o modificando directamente el código.