from pylatex import Document, Tabular, MultiColumn, MultiRow, Center
from pylatex.utils import NoEscape
from pylatex.basic import LargeText, LineBreak
from pylatex.section import Subsubsection
import os

import pandas as pd

def ficha(
    titulo
):
    pass

if __name__ == '__main__':

    #Lee el archivo talleres.csv
    df = pd.read_csv('talleres.csv', header=0)

    #Ordena las actividades por tipo
    df = df.sort_values(by='Tipo de actividad')
    
    #Declara los márgenes que tendrán las páginas del PDF
    geometry_options = {
        "tmargin":"1.75cm",
        "bmargin":"2cm"
    }

    doc = Document(geometry_options=geometry_options)

    #Estilo de la página, empty es sin numeración de página
    doc.preamble.append(NoEscape(r'\pagestyle{empty}'))
    
    for i in range(len(df.index)):
        doc.append(NoEscape(r'\newpage'))
        ########### ESTA ES LA CABECERA DE LAS FICHAS
        with doc.create(Center()):
            titu=r'\textbf{'+df['Titulo'][i]+r'}'
            doc.append(LargeText(NoEscape(titu)))
            doc.append(LineBreak())
            doc.append(df['Tipo de actividad'][i])
            doc.append(LineBreak())
            author = r'\textbf{'+df['Nombre completo'][i]+r'}'
            doc.append(NoEscape(author))
            doc.append(LineBreak())
            mail = r'\url{'+df['Correo'][i]+r'}'
            #doc.append(NoEscape(mail)) #Estas líneas producen error, probablemente por el arroba
            #print(mail)

        ############## CUERPO DE LAS FICHAS
        with doc.create(
            Subsubsection('Ejes temáticos',numbering=False)
        ):
            doc.append(df['Ejes'][i])
        
        with doc.create(
            Subsubsection('Público',numbering=False)
        ):
            doc.append(df['Publico'][i])
        
        with doc.create(
            Subsubsection('Duración',numbering=False)
        ):
            dura=str(df['Duracion'][i])+' minutos'
            doc.append(dura)
        
        with doc.create(
            Subsubsection(
                'Objetivo',
                numbering=False
            )
        ):
            doc.append(df['Objetivo'][i])
        
        with doc.create(
            Subsubsection(
                'Resumen',
                numbering=False
            )
        ):
            doc.append(df['Resumen'][i])
        
        with doc.create(
            Subsubsection(
                'Material',
                numbering=False
            )
        ):
            doc.append(df['Material'][i])
        
        with doc.create(
            Subsubsection(
                'Requerimientos técnicos',
                numbering=False
            )
        ):
            doc.append(df['Requerimientos'][i])
        
        with doc.create(
            Subsubsection(
                'Desarrollo',
                numbering=False
            )
        ):
            doc.append(df['Desarrollo'][i])
        
        with doc.create(
            Subsubsection(
                'Comentarios',
                numbering=False
            )
        ):
            doc.append(df['Comentarios'][i])
        
        with doc.create(
            Subsubsection(
                'Ponentes',
                numbering=False
            )
        ):
            doc.append(df['Num ponentes'][i])

#Genera el pdf y un archivo tex
#si clean_tex=True el archivo tex será borrado después de su compilación
doc.generate_pdf('CompendioTalleres',clean_tex=False)
